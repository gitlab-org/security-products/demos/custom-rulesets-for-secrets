
// the following secrets should be detected by gitleaks but NOT visible in Vulnerability Report section as they're disabled in the custom-ruleset file(.gitlab/secret-detection-ruleset.toml)

secret_ionic_token = "ion_123456789012345678901234567890123456789012" 
secret_slack_webhook_url = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"


// following secrets should be detected and visible in the Vulnerability Report as they're not disabled/overridden in the custom ruleset file

secret_gitlab_token= "glpat-12345123451234512345" // should detect and visible in vuln report
