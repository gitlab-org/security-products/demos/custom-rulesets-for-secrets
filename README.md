**This project is archived. Check projects under [Modify Default Ruleset](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset) for up to date demo projects.**

# custom-rulesets-for-secrets

This is an example project to demonstrate how you can create a custom ruleset
on top of the default rule-set that is shipped with the GitLab secrets
analyzer.

As outlined [here](https://docs.gitlab.com/ee/user/application_security/secret_detection/#custom-rulesets), this project adds a custom ruleset file named `secret-detection-ruleset.toml` in the `.gitlab` directory which is present (if one doesn't already exist) at the root of the project directory.

The add ruleset file demonstrates how to disable two rules from the default rule-set that is shipped with the GitLab secrets analyzer.
